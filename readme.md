# Code structure

Application is divided into 2 parts:
    app - django based backed application
    website - react based front end site

# Installation and running

## A. BACKEND

1. Install requirements for the app listed in requirements.txt (Only latest Django).
2. Run migrations: `python manage.py migrate`
3. Run the django app: `python manage.py runserver`

## B. FRONTEND

1. Install required packages by running: `npm install` within frontend directory.
2. Within the same directory start the front end server: `npm start`

Now the app is running and accessible under `http://localhost:3000`

# Description of the architecture

I've put more effort working on back end side, which is properly structured according to Model, View, Controller pattern. Additionally, I've also added service layer to separate out connections to 3rd party APIs.
Models are just simple Django models that allow us to use Django ORM.
Contollers contain all of the business logic of the application, gluing together data from 3rd party APIs and local database.
Views are just simple interfaces for the controllers.
I'm using very basic authentication method - simple cookie containing userID, to make website more straight forward and not require sign up and login.
I'm also saving searched results in the database to speed up the interface.

Improvements I'd like to add if given more time:
- Better authentication
- Checking and periodically updating saved searches, ideally using some separate in-memory database like redis
- Add error handling, which is almost non existent currently
- Add support for more fields coming from the 3rd party APIs

Front end is very quick and dirty. I haven't even had time to write tests for it.
There is a lot of space for improvement:
- Write everything in typescript, and sass
- Use redux for state management

Overall I would also like to add dockerized deployment for easier running of the application.