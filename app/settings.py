import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'de=m!8)6c9#$_)f7^-ng@(%t3y46etqxqgvlz&3g2(qgsd0p6&'
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'app'
]

MIDDLEWARE = []

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = 'static/'

ALPHAVANTAGE_API_KEY = 'DN64PYL068UCQNZ4'
