from app import settings
import urllib.request
import urllib.parse
import json


def suggest(query):
    '''
    Query suggest endpoint on clearbit autocomplete API.
    '''

    return json.loads(
        urllib.request.urlopen(
            'https://autocomplete.clearbit.com/v1/companies/suggest?query=' +
            urllib.parse.quote(query)).read())
