from app import settings
import urllib.request
import json
import re


def paramsToString(params):
    '''
    Convert params dict to a query string.
    '''

    return '&'.join(
        ['='.join(
            [param, str(value)]) for param, value in params.items()])


def stripNumbersFromKeys(inDict):
    '''
    Strips prefixing numbers from keys in a dict.
    '''

    return {re.sub('\d+\. ', '', key): value for key, value in inDict.items()}


def query(function, params):
    '''
    Query specified alphavantage api function with
    parameters contained in params dict.
    '''

    url = 'https://www.alphavantage.co/query?apikey=' + \
        settings.ALPHAVANTAGE_API_KEY + \
        '&function=' + function

    if params:
        url += '&' + paramsToString(params)

    return json.loads(urllib.request.urlopen(url).read())


def search(keywords):
    '''
    Query SYMBOL_SEARCH with specified keywords.
    '''

    response = query('SYMBOL_SEARCH', {'keywords': keywords})

    key = 'bestMatches'

    if key in response.keys():
        return response[key]


def quote(symbol):
    '''
    Query GLOBAL_QUOTE for specified symbol.
    '''

    response = query('GLOBAL_QUOTE', {'symbol': symbol})

    key = 'Global Quote'

    if key in response.keys():
        return response[key]


def daily(symbol, outputsize='compact'):
    '''
    Query TIME_SERIES_DAILY for specified symbol.
    '''

    response = query('TIME_SERIES_DAILY',
        { 'symbol': symbol, 'outputsize': outputsize })

    key = 'Time Series (Daily)'

    if key in response.keys():
        return response[key]


def intraday(symbol, outputsize='compact', interval='5min'):
    '''
    Query TIME_SERIES_INTRADAY for specified symbol.
    '''

    response = query('TIME_SERIES_INTRADAY',
        { 'symbol': symbol, 'outputsize': outputsize, 'interval': interval })

    key = 'Time Series (' + interval + ')'

    if key in response.keys():
        return response[key]
