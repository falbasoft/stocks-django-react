import uuid
from datetime import datetime
from django.test import TestCase

from app.controllers import quote
from app.models.company import Company
from app.models.quote import UserQuote, Quote


class TestCompanyController(TestCase):
    def add_company(self):
        company = Company(
            symbol='symbol',
            name='name',
            domain='domain',
            logo='logo')
        
        company.save()

        return company

    
    def add_quote(self, company):
        quote = Quote(
            quoteID=uuid.uuid4(),
            company=company,
            open='1.0000',
            high='1.0000',
            low='1.0000',
            price='1.0000',
            volume=1000,
            date=datetime.fromisoformat('2020-05-17'),
            previous_close='1.0000',
            change='0.0000',
            change_percent='0.0000'
        )

        quote.save()

        return quote


    def add_user_quote(self, userID):
        company = self.add_company()
        quote = self.add_quote(company)

        user_quote = UserQuote(quote=quote, userID=userID)
        user_quote.save()

        return user_quote


    def test_get_all_user_quotes(self):
        '''
        Test getting all user quotes.
        '''

        userID = uuid.uuid4()
        
        quotes = quote.get_all_user_quotes(userID)
        self.assertEquals(len(quotes), 0)

        self.add_user_quote(userID)

        quotes = quote.get_all_user_quotes(userID)
        self.assertEquals(len(quotes), 1)


    def test_add_company(self):
        '''
        Test adding company.
        '''

        def mock_company(symbol):
            return {
                'company': {
                    'name': 'name',
                    'domain': 'domain',
                    'logo': 'logo'
                }
            }

        quote.companyController.company = mock_company

        quote.add_company('symbol')

        try:
            added_company = Company.objects.get(symbol='symbol')
            self.assertEquals(added_company.name, 'name')

        except Company.DoesNotExist:
            self.fail('Company has not been added')
    
    
    def test_add_quote(self):
        '''
        Test adding quote.
        '''

        added_company = self.add_company()

        quote.add_quote(added_company, {
            'symbol': added_company.symbol,
            'open': '1.0000',
            'high': '1.0000',
            'low': '1.0000',
            'price': '1.0000',
            'volume': 1000,
            'latest trading day': '2020-05-17',
            'previous close': '1.0000',
            'change': '0.0000',
            'change percent': '0.0000%',
        })

        try:
            added_quote = Quote.objects.get(company=added_company)
            self.assertEquals(added_quote.company, added_company)
        
        except Quote.DoesNotExist:
            self.fail('Quote has not been added')


    def test_add_user_quote(self):
        '''
        Test adding user quote.
        '''

        userID = uuid.uuid4()

        quote.add_user_quote(userID, {
            'symbol': 'symbol',
            'open': '1.0000',
            'high': '1.0000',
            'low': '1.0000',
            'price': '1.0000',
            'volume': 1000,
            'latest trading day': '2020-05-17',
            'previous close': '1.0000',
            'change': '0.0000',
            'change percent': '0.0000%',
        })

        try:
            added_user_quote = UserQuote.objects.get(userID=userID)
            self.assertTrue(added_user_quote)
        
        except Quote.DoesNotExist:
            self.fail('Quote has not been added')

    
    def test_get_user_quote(self):
        '''
        Test getting user quote.
        '''

        userID = uuid.uuid4()
        
        user_quote = self.add_user_quote(userID)

        got_user_quote = quote.get_user_quote(userID, user_quote.quote)

        self.assertTrue(got_user_quote)

    
    def test_delete_user_quote(self):
        '''
        test deleting user quote
        '''

        userID = uuid.uuid4()
        
        user_quote = self.add_user_quote(userID)

        quote.delete_user_quote(userID, user_quote.quote)

        user_quotes = UserQuote.objects.filter(userID=userID).all()

        self.assertEquals(len(user_quotes), 0)
