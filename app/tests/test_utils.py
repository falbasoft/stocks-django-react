from django.test import TestCase
from app.utils import suffixes

class TestUtils(TestCase):
    def test_suffix_remove(self):
        '''
        Test removing suffixes.
        '''

        dirty = ['string ' + suffix for suffix in suffixes.suffixes]

        for string in dirty:
            clean = suffixes.remove(string)

            for suffix in suffixes.suffixes:
                self.assertNotRegexpMatches(clean, suffix)
