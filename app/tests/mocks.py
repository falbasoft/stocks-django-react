import json


class Response():
    '''
    Mock response that echoes request url into response.
    '''

    def __init__(self, url):
        super().__init__()
        self.url = url

    def read(self):
        return json.dumps(self.url)


def urlopen(url):
    '''
    Mock urlopen to return mock response.
    '''

    return Response(url)
