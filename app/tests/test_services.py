from django.test import TestCase

from . import mocks

from app.services import alphavantage, clearbit
from app.settings import ALPHAVANTAGE_API_KEY


class TestAlphavantage(TestCase):
    def test_alphavantage_params_to_string(self):
        '''
        Test converting params dict to query string.
        '''

        input_dict = {'keywords': 'facebook', 'limit': 100}
        expected_string = 'keywords=facebook&limit=100'

        self.assertEquals(
            alphavantage.paramsToString(input_dict), expected_string)


    def test_alphavantage_strip_numbers_from_keys(self):
        '''
        Test stripping numbers from keys in dict.
        '''

        dirty = {
            '1. name': 'some name',
            '2. domain': 'some domain'
        }

        clean = {
            'name': 'some name',
            'domain': 'some domain'
        }

        self.assertEquals(
            alphavantage.stripNumbersFromKeys(dirty), clean)
    

    def test_alphavantage_query(self):
        '''
        Test querying alphavantage API.
        '''

        alphavantage.urllib.request.urlopen = mocks.urlopen

        response = alphavantage.query('test', {'param': 'value'})
        expectedUrl = 'https://www.alphavantage.co/query?apikey=' + \
            ALPHAVANTAGE_API_KEY + '&function=test&param=value'

        self.assertEquals(response, expectedUrl)


class TestClearbit(TestCase):
    def test_clearbit_suggest(self):
        '''
        Test querying clearbit suggest API.
        '''

        clearbit.urllib.request.urlopen = mocks.urlopen

        response = clearbit.suggest('facebook')
        expectedUrl = 'https://autocomplete.clearbit.com/v1/' + \
            'companies/suggest?query=facebook'

        self.assertEquals(response, expectedUrl)
