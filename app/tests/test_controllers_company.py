from django.test import TestCase

from app.controllers import company


class TestCompanyController(TestCase):
    def test_search(self):
        '''
        Test calling search endpoint.
        '''

        def mock_search(keywords):
            return 'requested:' + keywords

        company.alphavantage.search = mock_search

        result = company.search('keywords')
        expected = 'requested:keywords'

        self.assertEquals(result, expected)

    
    def test_company(self):
        '''
        Test combining alphavantage matches with
        clearbit suggestions.
        '''

        def mock_search(symbol):
            return [{ '1. name': symbol }]
        
        def mock_quote(symbol):
            return { '1. high': '1.0000' }

        def mock_suggest(company):
            return [{ 'name': company }]

        suggest = company.clearbit.suggest

        company.alphavantage.search = mock_search
        company.alphavantage.quote = mock_quote
        company.clearbit.suggest = mock_suggest

        result = company.company('symbol')
        expected = {
            'company': {
                'name': 'symbol'
            },
            'quote': {
                'high': '1.0000'
            }
        }

        self.assertEquals(result, expected)

        company.clearbit.suggest = suggest


    def test_daily(self):
        '''
        Test getting list of daily quotes.
        '''

        def mock_daily(symbol, outputsize=None):
            return {
                '2020-05-17': {
                    '1. high': '1.0000'
                },
                '2020-05-18': {
                    '1. high': '1.2500'
                }
            }

        company.alphavantage.daily = mock_daily

        response = company.daily('symbol', {})
        expected = {
            '2020-05-17': {
                'high': '1.0000'
            },
            '2020-05-18': {
                'high': '1.2500'
            }
        }

        self.assertEquals(response, expected)


    def test_intraday(self):
        '''
        Test getting list of intraday quotes.
        '''

        def mock_intraday(symbol, outputsize=None):
            return {
                '2020-05-17 16:00:00': {
                    '1. high': '1.0000'
                },
                '2020-05-17 16:05:00': {
                    '1. high': '1.2500'
                }
            }

        company.alphavantage.intraday = mock_intraday

        response = company.intraday('symbol', {})
        expected = {
            '2020-05-17 16:00:00': {
                'high': '1.0000'
            },
            '2020-05-17 16:05:00': {
                'high': '1.2500'
            }
        }

        self.assertEquals(response, expected)
