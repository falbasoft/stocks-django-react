import re

suffixes = ['lp', 'llp', 'lllp', 'llc', 'lc', 'ltd. co.',
            'pllc', 'corp.', 'inc.', 'pc', 'p.c.']


def remove(string):
    '''
    Remove known American company suffixes from string.
    '''

    return re.sub(' (' + '|'.join(suffixes) + ')$', '', string)
