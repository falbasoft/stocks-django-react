from app.services import alphavantage, clearbit
from app.utils import suffixes


def search(keywords):
    '''
    A proxy for alphavantage search API.
    '''

    return alphavantage.search(keywords)


def company(symbol):
    '''
    Company controller combines search results with quote
    information coming from alphavantage API with
    company details coming from clearbit API.
    '''

    matches = alphavantage.search(symbol)

    if len(matches):
        stripped = alphavantage.stripNumbersFromKeys(matches[0])
        name = suffixes.remove(stripped['name'].lower())
        suggestions = clearbit.suggest(name)

        if len(suggestions):
            company = suggestions[0]
            quote = alphavantage.stripNumbersFromKeys(
                alphavantage.quote(symbol))

            return {
                'company': company,
                'quote': quote
            }

    return {}


def daily(symbol, params):
    '''
    Get list of company's daily quotes.
    '''

    args = { 'symbol': symbol }

    outputsize = params.get('outputsize')

    if outputsize:
        args['outputsize'] = outputsize

    quotes = alphavantage.daily(**args)

    if len(quotes):
        stripped = {
            date: alphavantage.stripNumbersFromKeys(quote)
                for date, quote in quotes.items()}

        return stripped


def intraday(symbol, params):
    '''
    Get list of company's intraday quotes.
    '''

    args = { 'symbol': symbol }

    outputsize = params.get('outputsize')

    if outputsize:
        args['outputsize'] = outputsize

    interval = params.get('interval')

    if interval:
        args['interval'] = interval

    quotes = alphavantage.intraday(**args)

    if len(quotes):
        stripped = {
            datetime: alphavantage.stripNumbersFromKeys(quote)
                for datetime, quote in quotes.items()}
        
        return stripped
