import uuid
from datetime import datetime
from django.forms.models import model_to_dict

from app.models.company import Company
from app.models.quote import UserQuote, Quote
from app.controllers import company as companyController


def get_all_user_quotes(userID):
    '''
    Returns a list of all quotes saved by the user.
    '''

    quotes = [model_to_dict(quote.quote) for quote in UserQuote.objects.filter(userID=userID).all()]

    return quotes


def add_company(symbol):
    '''
    Inserts company into database if it doesn't exist.
    '''

    try:
        company = Company.objects.get(symbol=symbol)

    except Company.DoesNotExist:
        companyDetails = companyController.company(symbol)
        
        company = Company(
            symbol=symbol,
            name=companyDetails['company']['name'],
            domain=companyDetails['company']['domain'],
            logo=companyDetails['company']['logo'])
        
        company.save()
    
    return company


def add_quote(company, quote):
    '''
    Inserts quote into database if it doesn't exist.
    '''

    try:
        quote = Quote.objects.get(
            company=quote['symbol'],
            date=datetime.fromisoformat(quote['latest trading day']))

    except Quote.DoesNotExist:
        quote = Quote(
            quoteID=uuid.uuid4(),
            company=company,
            open=quote['open'],
            high=quote['high'],
            low=quote['low'],
            price=quote['price'],
            volume=quote['volume'],
            date=datetime.fromisoformat(quote['latest trading day']),
            previous_close=quote['previous close'],
            change=quote['change'],
            change_percent=quote['change percent'].replace('%', '')
        )

        quote.save()
    
    return quote


def add_user_quote(userID, quote):
    '''
    Saves a quote for the user.
    '''

    company = add_company(quote['symbol'])
    quote = add_quote(company, quote)

    # check if user has this quote saved already and
    # save if necessary

    try:
        user_quote = UserQuote.objects.get(quote=quote, userID=userID)
    
    except UserQuote.DoesNotExist:
        user_quote = UserQuote(quote=quote, userID=userID)
        user_quote.save()

    return model_to_dict(user_quote.quote)


def get_user_quote(userID, quoteID):
    '''
    Gets a single quote saved by the user.
    '''

    try:
        user_quote = UserQuote.objects.get(quote=quoteID, userID=userID)

        return model_to_dict(user_quote.quote)

    except UserQuote.DoesNotExist:
        return {}


def delete_user_quote(userID, quoteID):
    '''
    Deletes a quote saved by the user.
    '''

    try:
        user_quote = UserQuote.objects.get(quote=quoteID, userID=userID)
        user_quote.delete()

        return model_to_dict(user_quote.quote)

    except UserQuote.DoesNotExist:
        return {}
