from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

from app.views import company, quote, website


urlpatterns = [
    path('', website.frontend),

    path('api/search/<str:keywords>', company.search),

    path('api/company/<str:symbol>', company.company),
    path('api/company/<str:symbol>/daily', company.company_daily),
    path('api/company/<str:symbol>/intraday', company.company_intraday),

    path('api/quotes', quote.quotes),
    path('api/quotes/<str:quoteID>', quote.quote)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
