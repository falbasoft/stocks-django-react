import json

from django.http import JsonResponse
from app.controllers import quote as controller


def quotes(request):
    '''
    This endpoint identifies user by his ID coming in a cookie.
    GET method returns a list of saved quotes.
    POST method saves a quote.
    '''

    userID = request.COOKIES.get('userID')
    print('userID:', userID)

    if request.method == 'GET':
        return JsonResponse(
            controller.get_all_user_quotes(userID), safe=False)
    
    if request.method == 'POST':
        return JsonResponse(
            controller.add_user_quote(userID, json.loads(request.body)))


def quote(request, quoteID):
    '''
    This endpoint identifies user by his ID coming in a cookie.
    GET method returns a saved quote.
    DELETE method removes a saved quote.
    '''

    userID = request.COOKIES.get('userID')

    if request.method == 'GET':
        return JsonResponse(
            controller.get_user_quote(userID, quoteID))

    if request.method == 'PUT':
        return JsonResponse(
            controller.update_user_quote(userID, request.body))

    if request.method == 'DELETE':
        return JsonResponse(
            controller.delete_user_quote(userID, quoteID))
