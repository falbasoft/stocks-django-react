from django.http import JsonResponse
from app.controllers import company as controller


def search(request, keywords):
    '''
    This endpoint takes keywords as an input and returns
    a list of matched companies.
    '''

    return JsonResponse(controller.search(keywords), safe=False)


def company(request, symbol):
    '''
    This endpoint takes company symbol as an input and returns
    company details and quote as an output.
    '''

    return JsonResponse(controller.company(symbol))


def company_daily(request, symbol):
    '''
    This endpoint takes company symbol as an input and returns
    company daily quotes as an output.
    Additional URL parameter:
    - outputsize: either 'compact' or 'full'
    '''

    return JsonResponse(
        controller.daily(symbol, request.GET))


def company_intraday(request, symbol):
    '''
    This endpoint takes company symbol as an input and returns
    company daily quotes as an output.
    Additional URL parameters:
    - outputsize: either 'compact' or 'full'
    - interval: one of '1min', '5min', '15min', '30min', '60min'
    '''

    return JsonResponse(
        controller.intraday(symbol, request.GET), safe=False)
