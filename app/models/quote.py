from django.db import models
from app.models import company


class Quote(models.Model):
    quoteID = models.UUIDField(primary_key=True)
    company = models.ForeignKey(company.Company, on_delete=models.CASCADE)
    open = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    price = models.FloatField()
    volume = models.IntegerField()
    date = models.DateField()
    previous_close = models.FloatField()
    change = models.FloatField()
    change_percent = models.FloatField()


class UserQuote(models.Model):
    quote = models.ForeignKey(Quote, on_delete=models.CASCADE)
    userID = models.UUIDField()
