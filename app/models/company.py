from django.db import models


class Company(models.Model):
    symbol = models.CharField(max_length=16, primary_key=True)
    name = models.CharField(max_length=128)
    domain = models.CharField(max_length=64)
    logo = models.CharField(max_length=128)