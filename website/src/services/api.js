import axios from 'axios';

export const company_details = async symbol => 
    (await axios.get(`/api/company/${symbol}`)).data;

export const quotes = async () =>
    (await axios.get(`/api/quotes`)).data;

export const quote_save = async quote =>
    (await axios.post(`/api/quotes`, quote)).data;

export const quote_remove = async quoteID =>
    (await axios.delete(`/api/quotes/${quoteID}`)).data;
