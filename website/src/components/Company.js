import React from 'react';

import { Button } from 'react-bootstrap';

import './Company.css';

const Company = props => {
  return (
    <div className="row w-100">
      <div className="col-12 col-md-6">
        <table className="company">
          <tbody>
            <tr>
              <td>price:</td>
              <td>{props.company.quote.price}</td>
            </tr>
            <tr>
              <td>volume:</td>
              <td>{props.company.quote.volume}</td>
            </tr>
            <tr>
              <td>low:</td>
              <td>{props.company.quote.low}</td>
            </tr>
            <tr>
              <td>high:</td>
              <td>{props.company.quote.high}</td>
            </tr>
            <tr>
              <td>open:</td>
              <td>{props.company.quote.open}</td>
            </tr>
            <tr>
              <td>previous close:</td>
              <td>{props.company.quote['previous close']}</td>
            </tr>
            <tr>
              <td>change:</td>
              <td>{props.company.quote.change}</td>
            </tr>
            <tr>
              <td>latest trading day:</td>
              <td>{props.company.quote['latest trading day']}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="col-12 col-md-6">
        <h2>{props.company.company.name}</h2>
        <img src={props.company.company.logo} alt="logo" />
        <table className="company">
          <tbody>
            <tr>
              <td>website:</td>
              <td>
                <a
                  href={`http://${props.company.company.domain}`}
                  target="_blank"
                >{props.company.company.domain}</a>
              </td>
            </tr>
            <tr>
              <td>symbol:</td>
              <td>{props.company.quote.symbol}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <Button className="col-6 mt-3 mx-auto" variant="success" onClick={props.save}>Save</Button>
    </div>)
}

export default Company;
