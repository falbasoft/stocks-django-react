import React, { useEffect, useState } from 'react';

import cookie from 'react-cookies';
import { v4 as uuidv4 } from 'uuid';

import { Button, Form, FormGroup, Spinner } from 'react-bootstrap';

import Company from './Company';
import Quotes from './Quotes';

import { company_details, quotes, quote_save } from '../services/api';

import './App.css';

const App = () => {
  const [symbol, setSymbol] = useState('');
  const [loading, setLoading] = useState(false);
  const [company, setCompany] = useState(undefined);
  const [saved, setSaved] = useState([]);

  let userID = cookie.load('userID');

  if (!userID) {
    userID = uuidv4();
    cookie.save('userID', userID);
  }

  useEffect(() => {reload()}, []);

  const reload = async () => setSaved(await quotes());

  const submit = async event => {
    event.preventDefault();

    setCompany(undefined);
    setLoading(true);

    const details = await company_details(symbol);

    setLoading(false);
    details['company'] && setCompany(details);
  }

  const save = async event => {
    event.preventDefault();

    await quote_save(company.quote);

    reload();
  }

  return (
    <div className="app d-flex align-items-center">
      <div className="container">
        <div className="row w-75 mx-auto">
          <Form className="w-100" onSubmit={submit}>
            <FormGroup controlId="company">
              <Form.Label className="w-100">Company Symbol:</Form.Label>

              <div className="d-flex">
                <Form.Control
                  type="symbol"
                  placeholder="Enter Symbol"
                  className="d-flex flex-grow-1"
                  autoComplete="off"
                  onChange={e => setSymbol(e.target.value)}
                />

                <Button className="ml-2 d-flex flex-grow-0" type="submit">Search</Button>
              </div>
            </FormGroup>
          </Form>
        </div>
        
        <div className="row w-75 mx-auto">
          {loading && (<Spinner animation="border" className="mx-auto" role="status">
                        <span className="sr-only">Loading...</span>
                      </Spinner>)}
          {company && <Company company={company} save={save}></Company>}
        </div>

        <div className="row w-100 mx-auto">
          <Quotes list={saved} reload={reload}></Quotes>
        </div>
      </div>
    </div>
  );
}

export default App;
