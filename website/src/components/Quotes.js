import React from 'react';

import { Button } from 'react-bootstrap';

import { quote_remove } from '../services/api';

import './Quotes.css';

const Quotes = props => {
    const remove = async (event, quoteID) => {
        event.preventDefault();

        await quote_remove(quoteID);

        props.reload();
    }

    return (
        <div className="quotes_container mx-auto">{
            props.list.map(quote => (
                <div className="quote" key={quote.quoteID}>
                    <table>
                        <tbody>
                            <tr>
                                <td>company:</td>
                                <td>{quote.company}</td>
                            </tr>
                            <tr>
                                <td>date:</td>
                                <td>{quote.date}</td>
                            </tr>
                            <tr>
                                <td>price:</td>
                                <td>{quote.price}</td>
                            </tr>
                            <tr>
                                <td>volume:</td>
                                <td>{quote.volume}</td>
                            </tr>
                            <tr>
                                <td>low:</td>
                                <td>{quote.low}</td>
                            </tr>
                            <tr>
                                <td>high:</td>
                                <td>{quote.high}</td>
                            </tr>
                        </tbody>
                    </table>

                    <Button
                        className="w-100 mt-2"
                        variant="danger"
                        onClick={event => remove(event, quote.quoteID) }
                    >Remove</Button>
                </div>
            ))
        }</div>
    )
}

export default Quotes;